<?php
namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use TheFeed\Controleur\ControleurUtilisateur;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use TheFeed\Lib\Conteneur;


class RouteurURL
{
    public static function traiterRequete() {
        $requete = Request::createFromGlobals();

        $routes = new RouteCollection();

        // Route afficherListe
        $route = new Route("/publications", ["_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",]);
        $route->setMethods(["GET"]);
        $routes->add("afficherListe", $route);

        // Route afficherListe Temp
        $route = new Route("/", ["_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",]);
        $routes->add("afficherListeTemp", $route);

        // Route afficherFormulaireConnexion
        $route = new Route("/connexion", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",]);
        $route->setMethods(["GET"]);
        $routes->add("afficherFormulaireConnexion", $route);

        // Syntaxes équivalentes
        // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
        // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],

        // Route connecter
        $route = new Route("/connexion", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",]);
        $route->setMethods(["POST"]);
        $routes->add("connecter", $route);

        // Route deconnecter
        $route = new Route("/deconnexion", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter",]);
        $route->setMethods(["GET"]);
        $routes->add("deconnecter", $route);

        // Route afficherFormulaireCreation
        $route = new Route("/inscription", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation",]);
        $route->setMethods(["GET"]);
        $routes->add("afficherFormulaireCreation", $route);

        // Route inscrit
        $route = new Route("/inscription", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire",]);
        $route->setMethods(["POST"]);
        $routes->add("inscrit", $route);

        // Route creer
        $route = new Route("/publications", ["_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",]);
        $route->setMethods(["POST"]);
        $routes->add("creer", $route);

        // Route afficherPublications
        $route = new Route("/utilisateurs/{idUtilisateur}/publications", ["_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherPublications",]);
        $route->setMethods(["GET"]);
        $routes->add("afficherPublications", $route);

        $contexteRequete = (new RequestContext())->fromRequest($requete);

        $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);

        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ControllerResolver();
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (MethodNotAllowedException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 405);
        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage(), 404);
        } catch (\Exception $exception) {
            $reponse = ControleurGenerique::afficherErreur($exception->getMessage()) ;
        }
        $reponse->send();
    }
}
